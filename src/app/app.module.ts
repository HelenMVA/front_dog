import { BrowserModule } from '@angular/platform-browser';


import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { HeaderComponent } from './components/header/header.component';
import { NavComponent } from './components/nav/nav.component';
import { AdoptSectionComponent } from './components/adopt-section/adopt-section.component';
import { AdoptPageComponent } from './pages/adopt-page/adopt-page.component';
import { CardsComponent } from './components/cards/cards.component';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdoptDogDescriptionComponent } from './pages/adopt-dog-description/adopt-dog-description.component';
import { AdoptFormComponent } from './components/adopt-form/adopt-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterModalComponent } from './components/register-modal/register-modal.component';
import { ConnectModalComponent } from './components/connect-modal/connect-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

//Angular Material Components
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MyProfilComponent } from './pages/my-profil/my-profil.component';
import { AdminComponent } from './pages/admin/admin.component';
import { ModalSendMessageComponent } from './components/modal-send-message/modal-send-message.component';
import { UserMessagesComponent } from './components/user-messages/user-messages.component';
import { UserAdoptRequestComponent } from './components/user-adopt-request/user-adopt-request.component';
import { ModalInfoComponent } from './components/modal-info/modal-info.component';
import { AdminAdoptComponent } from './components/admin-adopt/admin-adopt.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { MainHomePageComponent } from './components/main-home-page/main-home-page.component';
import { FooterComponent } from './components/footer/footer.component';
import { ModalAddAdoptAnnounceAdminComponent } from './components/modal-add-adopt-announce-admin/modal-add-adopt-announce-admin.component';
import { GardePageComponent } from './pages/garde-page/garde-page.component';
import { ModaleGardeUserAnnonceComponent } from './components/modale-garde-user-annonce/modale-garde-user-annonce.component';
import { MatNativeDateModule } from '@angular/material/core';
import { PipeModule } from './pipes/pipe.module';
import { UserAnnoncesComponent } from './components/user-annonces/user-annonces.component';
import { UserDataComponent } from './components/user-data/user-data.component';
import { ConfirmModalComponent } from './components/modals/confirm-modal/confirm-modal.component';
import { CurrentAnnonceComponent } from './components/user-annonces/current-annonce/current-annonce.component';
import { PastAnnonceComponent } from './components/user-annonces/past-annonce/past-annonce.component';
import { DatePipe } from '@angular/common';
import { UserKeepRequestComponent } from './components/user-keep-request/user-keep-request.component';
import { CalendarActivityComponent } from './components/calendar-activity/calendar-activity.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { ModalUserEditAnnonceComponent } from './components/modal-user-edit-annonce/modal-user-edit-annonce.component';

import { JwtInterceptor } from "./helpers/auth.interceptor";

FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin
]);


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    NavComponent,
    AdoptSectionComponent,
    AdoptPageComponent,
    CardsComponent,
    AdoptDogDescriptionComponent,
    AdoptFormComponent,
    RegisterModalComponent,
    ConnectModalComponent,
    MyProfilComponent,
    AdminComponent,
    ModalSendMessageComponent,
    UserMessagesComponent,
    UserAdoptRequestComponent,
    ModalInfoComponent,
    AdminAdoptComponent,
    AdminUsersComponent,
    MainHomePageComponent,
    FooterComponent,
    ModalAddAdoptAnnounceAdminComponent,
    GardePageComponent,
    ModaleGardeUserAnnonceComponent,
    UserAnnoncesComponent,
    UserDataComponent,
    ConfirmModalComponent,
    CurrentAnnonceComponent,
    PastAnnonceComponent,
    UserKeepRequestComponent,
    CalendarActivityComponent,
    ModalUserEditAnnonceComponent,
  ],
  exports: [
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    // import angular material modules
    MatCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatNativeDateModule,
    PipeModule,
    FormsModule,
    ReactiveFormsModule,
    FullCalendarModule,
  ],
  providers: [HttpClient,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    , NgbActiveModal, DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule { }
