import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AdoptService {
  apiURL = `${environment.API_URL}/back/`;
  constructor(private http: HttpClient) { }

  getAdoptionDogs() {
    return this.http.get(`${this.apiURL}adoption`);
  }
  adopter(dog) {
    return this.http.post(`${this.apiURL}adoption/request`, dog);
  }
}
