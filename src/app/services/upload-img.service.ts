import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment"
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';


@Injectable({
    providedIn: 'root'
})
export class UploadImgService {
    apiURL = `${environment.API_URL}/back/`;
    FOLDER = 'furry_friends/';
    myUrl: string;


    constructor(private http: HttpClient) {
    }

    /**
     *  UPLOAD img keep  dog annonce
     * */
    uploadFile(file, key) {
        return new Promise((resolve, reject) => {
            const contentType = file.type;
            const bucket = new S3(
                {
                    accessKeyId: environment.ACCESS_KEY,
                    secretAccessKey: environment.SECRET_KEY,
                    region: 'eu-west-3'
                }
            );
            const params = {
                Bucket: 'alyona',
                Key: key,
                Body: file,
                ACL: 'public-read',
                ContentType: contentType
            };
            return bucket.upload(params, (err, data) => {
                if (err) {
                    reject(err);
                }
                resolve(data);
            });
        });
    }
}
