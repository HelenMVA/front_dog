import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RateService {
  apiURL = `${environment.API_URL}/back/rate/`;

  constructor(private http: HttpClient) {
  }


  sendUserRate(body) {
    return this.http.post(`${this.apiURL}${body.id_demand}`, body);
  }

  getAllUserRating(serve_user_id) {
    return this.http.get(`${this.apiURL}user/${serve_user_id}`);
  }

  updateGlobalUserRate(serve_user_id, body) {
    return this.http.put(`${this.apiURL}user/${serve_user_id}`, body);
  }
  getTopRateUser() {
    return this.http.get(`${this.apiURL}top3`);
  }
}
