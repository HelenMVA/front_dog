import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserPassAnnoncesService {
  apiURL = `${environment.API_URL}/back/`;
  constructor(private http: HttpClient) { }


  getUserPassAnnonces(owner_id, body) {
    return this.http.post(`${this.apiURL}users/passAnnonces/${owner_id}`, body);
  }

  findRateByUser(body) {
    return this.http.post(`${this.apiURL}users/findRateByUser`, body);
  }
}
