import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  apiURL = `${environment.API_URL}/back/`;
  constructor(private http: HttpClient) { }
  getAllDemandes() {
    return this.http.get(`${this.apiURL}admin`);
  }

  //* ACCEPT /REJECT demand adoptation//

  accept(id) {
    return this.http.put(`${this.apiURL}admin/accept/${id}`, 'accept');
  }
  changeAvailableDog(id, dog_avalaible) {
    return this.http.put(`${this.apiURL}admin/availableDog/${id}`, dog_avalaible);
  }
  reject(id) {
    return this.http.put(`${this.apiURL}admin/reject/${id}`, 'reject');
  }
  sendUploadImg(annonce) {
    return this.http.post(`${this.apiURL}admin/annonce`, annonce);
  }



  getAllUsers() {
    return this.http.get(`${this.apiURL}admin/allUsers`);
  }

  blockedUser(data, id) {
    return this.http.put(`${this.apiURL}admin/blockedUser/${id}`, data);
  }

  getAdmin() {
    return this.http.get(`${this.apiURL}admin/data`);
  }
  addAnnonce(annonce) {
    console.dir(annonce);
    return this.http.post(`${this.apiURL}admin/nawannonce`, annonce);
  }
}
