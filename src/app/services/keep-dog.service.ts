import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class KeepDogService {
  apiURL = `${environment.API_URL}/back/keep/`;

  constructor(private http: HttpClient) { }

  addAnnonce(annonce) {
    return this.http.post(`${this.apiURL}annonce`, annonce);
  }
  getKeepDogs(body) {
    return this.http.post(`${this.apiURL}dogs`, body);
  }

  sendDemand(demand) {
    return this.http.post(`${this.apiURL}keepDog`, demand);
  }
  deleteMyAnnonce(body) {
    return this.http.post(`${this.apiURL}deleteAnnonce/${body.dog_id}`, body);
  }
}
