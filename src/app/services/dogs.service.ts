import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DogsService {
  apiURL = `${environment.API_URL}/back/adoption/`;
  constructor(private http: HttpClient) { }

  getDog(slug) {
    return this.http.get(`${this.apiURL}${slug}`);
  }
}
