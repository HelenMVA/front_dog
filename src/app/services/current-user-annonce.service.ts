import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserAnnonceService {
  apiURL = `${environment.API_URL}/back/`;
  constructor(private http: HttpClient) { }


  getUserCurrentAnnonces(owner_id, body) {
    return this.http.post(`${this.apiURL}users/currentAnnonces/${owner_id}`, body);
  }

  getDemandeByAnnonce(dog_id) {
    return this.http.get(`${this.apiURL}users/currentDemandes/${dog_id}`);
  }

  editStatusDemand(body) {
    return this.http.put(`${this.apiURL}users/keepDemand/editStatus`, body);
  }
  editDogAvailability(body) {
    return this.http.put(`${this.apiURL}users/currentAnnonces/availability`, body);
  }
}
