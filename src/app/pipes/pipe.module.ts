import { NgModule } from '@angular/core';
import { SafePipe } from './url-safe.pipe';
import { DisplayRatePipe } from './display-rate.pipe';
@NgModule({
    imports: [],
    declarations: [
        SafePipe,
        DisplayRatePipe
    ],
    exports: [
        SafePipe,
        DisplayRatePipe
    ],
    providers: [

    ]
})
export class PipeModule {

}
