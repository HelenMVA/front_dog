import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayRate'
})
export class DisplayRatePipe implements PipeTransform {

  transform(averageRate: any): Array<any> {
    const valueAverageRate: any = averageRate ? Math.ceil(averageRate / 0.5) * 0.5 : 0;
    let starsArray: Array<any> = Array(5).fill(null);


    if (valueAverageRate === 0) {
      starsArray = starsArray.map((index) => {
        return {
          classIcon: 'far fa-star'
        };
      });
    }
    else if (!!(valueAverageRate % 1)) {
      starsArray = starsArray.map((rate, index) => {
        return {
          classIcon:
            index === valueAverageRate - 0.5 ? 'fas fa-star-half yellow' :
              valueAverageRate > index ? 'fas fa-star yellow' :
                valueAverageRate < index ? 'far fa-star' :
                  null
        };
      });
    }
    else {
      starsArray = starsArray.map((rate, index) => {
        return {
          classIcon:
            valueAverageRate > index ? 'fas fa-star yellow' : 'far fa-star',
        };
      });
    }
    return starsArray;
  }

}
