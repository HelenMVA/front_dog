import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';
import { MyUsers } from '../../Models/myUsers';
import { UserMessages } from '../../Models/userMessages';

@Component({
  selector: 'app-user-messages',
  templateUrl: './user-messages.component.html',
  styleUrls: ['./user-messages.component.scss'],
})
export class UserMessagesComponent implements OnInit, OnDestroy {
  FormMessage: FormGroup = this.fb.group({
    message: ['', Validators.required],
  });

  currentUserSubscription: Subscription;
  currentUser;
  userId: string;
  userMessages: UserMessages[] = [];
  myUsers: MyUsers[] = [];

  fromUserId: string;

  constructor(private userService: UserService,
    private currentUserService: CurrentUserService,
    private fb: FormBuilder) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user: any) => {
        if (user != null) {
          this.currentUser = user;
          this.userId = user.data.user.id;
          this.getMyUsers(this.userId);
        }
      }
    );
  }

  ngOnInit(): void { }
  getMyUsers(userId) {
    return this.userService.getMyUsers(userId).subscribe((res: any) => {
      if (res && res.data.length > 0) {
        this.myUsers = res.data;
        this.myUsers.forEach((myUser: any) =>
          this.getUserMessage(myUser.id));
      } else {
        this.myUsers = [];
      }

    });
  }

  getUserMessage(fromUserId) {
    this.fromUserId = fromUserId;
    /**
     * on mets user active true
     * pour utiliser class active css
     */
    console.dir(this.myUsers);
    this.myUsers.forEach(usr => {
      if (usr.id === fromUserId) {
        usr.active = true;
      } else {
        usr.active = false;
      }
    });
    return this.userService
      .getUserMessage(fromUserId, this.userId)
      .subscribe((res: any) => {
        this.userMessages = res.data;
      });
  }
  sendMessage() {
    const fromUserId = this.userId;
    const toUserId = this.fromUserId;
    const message = this.FormMessage.value.message;
    const sendMessage = {
      fromUserId,
      toUserId,
      message,
    };
    this.userService.sendMessage(sendMessage).subscribe(
      (data) => {
        this.FormMessage.reset();
        this.getUserMessage(this.fromUserId);
      },
      (err) => {
        console.dir(err);
      }
    );
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
