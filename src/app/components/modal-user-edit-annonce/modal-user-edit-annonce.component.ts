import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DropDown } from "../../Models/drop-down";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { UploadImgService } from "../../services/upload-img.service";
import { KeepDogService } from "../../services/keep-dog.service";
import { KeepDog } from "../../Models/keepDog";

@Component({
  selector: 'app-modal-user-edit-annonce',
  templateUrl: './modal-user-edit-annonce.component.html',
  styleUrls: ['./modal-user-edit-annonce.component.scss']
})
export class ModalUserEditAnnonceComponent implements OnInit {
  @Input() public currentUser;
  @Input() public annonce: KeepDog;
  newAnnonce: FormGroup = this.fb.group({
    dogName: ['', Validators.compose([Validators.required])],
    age: ['', Validators.compose([Validators.required])],
    description: ['', Validators.compose([Validators.required])],
    race: ['', Validators.compose([Validators.required])],
    prix: ['', Validators.compose([Validators.required])],
    address: ['', Validators.compose([Validators.required])],
    url: ['', Validators.compose([Validators.required])],
    class: ['', Validators.compose([Validators.required])],
    catLove: ['', Validators.compose([Validators.required])],
    dogLove: ['', Validators.compose([Validators.required])],
    childLove: ['', Validators.compose([Validators.required])],
    sexe: ['', Validators.compose([Validators.required])],
    start: ['', Validators.compose([Validators.required])],
    end: ['', Validators.compose([Validators.required])],
  });

  selectedFiles: FileList;
  file;
  FOLDER = 'furry_friends/';
  filekey: string;
  fileUrl = '  https://alyona.s3.eu-west-3.amazonaws.com/';

  classification: DropDown[];
  dogSexe: DropDown[];
  atherDog: DropDown[];
  cats: DropDown[];
  childs: DropDown[];
  minDate = new Date();
  maxDate = new Date(2020, 0, 1);
  startDate: Date;
  finDate: Date;
  notStartDate = true;

  constructor(private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private imageUploadService: UploadImgService,
    private keepDogService: KeepDogService
  ) {

    this.classification = [
      { value: '1', viewValue: 'Chiot' },
      { value: '2', viewValue: 'Junior' },
      { value: '3', viewValue: 'Adulte' },
      { value: '4', viewValue: 'Senior' },
    ];
    this.dogSexe = [
      { value: '1', viewValue: 'Male' },
      { value: '2', viewValue: 'Femelle' },
    ];
    this.atherDog = [
      { value: '0', viewValue: "Incompatible avec d' autres chiens " },
      { value: '1', viewValue: 'Compatible' },
    ];
    this.cats = [
      { value: '0', viewValue: 'Incompatible' },
      { value: '1', viewValue: 'Compatible' },
    ];
    this.childs = [
      { value: '0', viewValue: 'Incompatible' },
      { value: '1', viewValue: 'Compatible' },
    ];
  }

  ngOnInit(): void {
    this.newAnnonce.controls.class.setValue(this.annonce.classification);
  }

  closeModal() {
    this.activeModal.close();
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.file = this.selectedFiles.item(0);
    this.filekey = this.FOLDER + (Date.now() + this.selectedFiles.item(0).name);
    this.newAnnonce.controls.url.setValue(this.fileUrl + this.filekey);
  }

  choseClassification(e) {
    // this.selectedClass = e.value.value,
    this.newAnnonce.controls.class.setValue(e.value.value);
  }

  choseDogSexe(e) {
    // this.selectedSexe = e.value.value,
    this.newAnnonce.controls.sexe.setValue(e.value.value);
  }

  choseLoveAutherDogs(e) {
    // this.dogLoveSelectes = e.value.value;
    this.newAnnonce.controls.dogLove.setValue(e.value.value);
  }

  choseLovaCat(e) {
    // this.catLoveSelected =e.value.value,
    this.newAnnonce.controls.catLove.setValue(e.value.value);
  }

  choseLoveChild(e) {
    // this.childLoveSelected = e.value.value;
    this.newAnnonce.controls.childLove.setValue(e.value.value);
  }

  datestart(e) {
    this.startDate = new Date(e.value);
    this.maxDate = this.startDate;
    this.notStartDate = false;
    this.newAnnonce.controls.start.setValue(this.startDate);
  }

  dateEnd(e) {
    this.finDate = new Date(e.value);
    this.newAnnonce.controls.end.setValue(this.finDate);
  }

  send() {
    this.imageUploadService.uploadFile(this.file, this.filekey).then(() => {
      const annonce = {
        name: this.newAnnonce.value.dogName,
        owner_id: this.currentUser.data.user.id,
        characteristic: this.newAnnonce.value.description,
        classification: this.newAnnonce.value.class,
        photo: this.newAnnonce.value.url,
        age: this.newAnnonce.value.age,
        prix: this.newAnnonce.value.prix,
        sexe: this.newAnnonce.value.sexe,
        race: this.newAnnonce.value.race,
        date_fin: this.getFormattedDate(this.finDate),
        date_start: this.getFormattedDate(this.startDate),
        slug: this.newAnnonce.value.dogName + Date.now(),
        address: this.newAnnonce.value.address,
        love_dog: this.newAnnonce.value.dogLove,
        love_cat: this.newAnnonce.value.catLove,
        love_child: this.newAnnonce.value.childLove,

      }
      this.keepDogService.addAnnonce(annonce).subscribe(
        (res) => {
          this.activeModal.close(res);
        },
        (err) => {
        }
      );
    });
  }

  getFormattedDate(date) {
    const year = date.getFullYear();
    const month = (1 + date.getMonth()).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');

    return year + '/' + month + '/' + day;
  }

}
