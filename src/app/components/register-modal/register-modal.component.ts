import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.scss']
})
export class RegisterModalComponent implements OnInit {
  FormRegister: FormGroup = this.fb.group({
    firstname: ['', Validators.compose([Validators.required])],
    lastname: ['', Validators.compose([Validators.required])],
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });


  constructor(private fb: FormBuilder,
    public toastService: ToastrService,
    public activeModal: NgbActiveModal,
    private userService: UserService) { }

  ngOnInit(): void {
  }
  closeModal() {
    this.activeModal.close();
  }
  send() {
    this.userService.register(this.FormRegister.value).subscribe(data => {
      this.toastService.success('Your account is successfully created', '');
      this.closeModal();
    }, (err) => {
      this.toastService.error('Еhis account already exists');
    }
    );

  }

}
