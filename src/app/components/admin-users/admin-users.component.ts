import { Component, OnInit } from '@angular/core';
import { User } from '../../Models/user';
import { AdminService } from "../../services/admin.service";
import { ModalSendMessageComponent } from "../modal-send-message/modal-send-message.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
import { ConfirmModalComponent } from "../modals/confirm-modal/confirm-modal.component";
import { UserService } from "../../services/user.service";


@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit {
  allUsers: User[] = [];

  visibility: number;
  blocked: number;
  constructor(private adminService: AdminService,
    private modalService: NgbModal,
    private userService: UserService,
    private toastService: ToastrService) {
  }

  ngOnInit(): void {
    this.getAllUsers();
  }


  getAllUsers() {
    this.adminService.getAllUsers().subscribe((res: any) => {
      this.allUsers = res.message;
    });
  }


  sendMessage(demand) {
    const modalRef = this.modalService.open(ModalSendMessageComponent);
    modalRef.componentInstance.userReceiverId = demand.id;
    modalRef.componentInstance.firstname = demand.firstname;
    modalRef.result.then((result) => {
      if (result && result != null) {
        this.toastService.success("votre message etait envoyé");
      }
    });
  }

  changeVisibility(e, user) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.text = "Voulez vous chager la visibilite de cette user";
    modalRef.result.then((result) => {
      if (result && result === 'yes') {
        if (e.checked === true) {
          this.visibility = 1;
        } else if (e.checked === false) {
          this.visibility = 0;
        }
        const body = {
          visibility: this.visibility,
        };
        this.userService.changeVisibility(body, user.id).subscribe((res: any) => {
          if (res && res.success) {
            user.visibility = e.checked;
            this.toastService.success('visibilite etait modifie');
          }
        });
      }
    });
  }

  changeBlocked(e, user) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    if (e.checked === true) {
      modalRef.componentInstance.text = "Voulez vous blocker cette user";
    } else {
      modalRef.componentInstance.text = "Voulez vous debloquer cette user";
    }

    modalRef.result.then((result) => {
      if (result && result === 'yes') {
        if (e.checked === true) {
          this.blocked = 1;
        } else if (e.checked === false) {
          this.blocked = 0;
        }
        const body = {
          blocked: this.blocked,
        };
        this.adminService.blockedUser(body, user.id).subscribe((res: any) => {
          if (res && res.success) {
            user.blocked = e.checked;
            if (e.checked === true) {
              this.toastService.success('User etait bloque');
            } else {
              this.toastService.success('User etait debloque');
            }
          }
        });
      }
    });
  }
}
