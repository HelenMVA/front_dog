import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleGardeUserAnnonceComponent } from './modale-garde-user-annonce.component';

describe('ModaleGardeUserAnnonceComponent', () => {
  let component: ModaleGardeUserAnnonceComponent;
  let fixture: ComponentFixture<ModaleGardeUserAnnonceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleGardeUserAnnonceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleGardeUserAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
