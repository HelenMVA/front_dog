import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserKeepRequestComponent } from './user-keep-request.component';

describe('UserKeepRequestComponent', () => {
  let component: UserKeepRequestComponent;
  let fixture: ComponentFixture<UserKeepRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserKeepRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserKeepRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
