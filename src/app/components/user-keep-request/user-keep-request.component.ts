import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { KeepDemand } from 'src/app/Models/keep-demand';
import { KeepDog } from 'src/app/Models/keepDog';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-keep-request',
  templateUrl: './user-keep-request.component.html',
  styleUrls: ['./user-keep-request.component.scss']
})
export class UserKeepRequestComponent implements OnInit {
  currentUserSubscription: Subscription;
  currentUser;
  cardLoiding: boolean;


  keepDogs: KeepDog[] = [];
  keepDogsAccept: KeepDemand[] = [];

  constructor(private userService: UserService,
    private currentUserService: CurrentUserService,
    private router: Router) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user) => {
        if (user != null) {
          this.currentUser = user;
          this.userKeepDogs();
        }
      }
    );
  }

  ngOnInit(): void {
  }

  userKeepDogs() {
    this.cardLoiding = true;
    this.userService.getUserKeepDogs(this.currentUser.data.user.id).subscribe((res: any) => {
      if (res && res.data !== 'No keep dog by this user') {
        this.keepDogs = res.data;
        this.checkAcceptDogs(res.data);

      } else {
        this.keepDogs = [];
      }
      this.cardLoiding = false;
    });
  }
  checkAcceptDogs(allDemandes) {
    this.keepDogsAccept = [];
    allDemandes.forEach((demand: KeepDemand) => {
      if (demand.status === 'accept') {
        this.keepDogsAccept.push(demand);
      }
    });
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
