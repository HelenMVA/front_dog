import { NgModule } from '@angular/core';
import { PipeModule } from '../../pipes/pipe.module';
import { CalendarActivityComponent } from './calendar-activity/calendar-activity.component';
import { RequestComponent } from './request/request.component';

@NgModule({
    imports: [
        PipeModule
    ],
    declarations: [

    CalendarActivityComponent,

    RequestComponent],
    providers: [

    ]
})
export class UserKeepRequestModule {
}