import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-modal-send-message',
  templateUrl: './modal-send-message.component.html',
  styleUrls: ['./modal-send-message.component.scss'],
})
export class ModalSendMessageComponent implements OnInit, OnDestroy {
  // @Input() public userReceiverId;
  @Input() public userReceiverId;
  @Input() public firstname;
  @Input() public dogname;
  @Input() public admin;


  currentUserSubscription: Subscription;

  userSender;

  FormMessage: FormGroup = this.fb.group({
    message: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private currentUserService: CurrentUserService,
    private userService: UserService
  ) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user) => {
        this.userSender = user;
      }
    );
  }

  ngOnInit(): void { }
  sendMessage() {
    const fromUserId = this.userSender.data.user.id;
    const toUserId = this.userReceiverId;
    const message = this.FormMessage.value.message;
    const sendMessage = {
      fromUserId,
      toUserId,
      message,
    };
    this.userService.sendMessage(sendMessage).subscribe(
      (data) => {
        this.activeModal.close(sendMessage);
      },
      (err) => {
        console.dir(err);
      }
    );
  }

  closeModal() {
    this.activeModal.close();
  }
  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
