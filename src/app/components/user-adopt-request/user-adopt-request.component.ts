import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-adopt-request',
  templateUrl: './user-adopt-request.component.html',
  styleUrls: ['./user-adopt-request.component.scss'],
})
export class UserAdoptRequestComponent implements OnInit, OnDestroy {
  currentUserSubscription: Subscription;
  currentUser;
  userId: string;
  userDogs: [];
  cardLoiding: boolean;
  constructor(private userService: UserService,
    private currentUSerService: CurrentUserService,
    private router: Router) {
    this.currentUserSubscription = this.currentUSerService.currentUser.subscribe(
      (user: any) => {
        if (user != null) {
          this.currentUser = user;
          this.userId = user.data.user.id;
          this.getUserDods(this.userId);
        }
      }
    );
  }

  ngOnInit(): void {
    window.scroll(0, 0);
  }
  getUserDods(id_user) {
    this.cardLoiding = true;
    return this.userService.getUserDods(id_user).subscribe((res: any) => {
      if (res.data === 'No dog by this user') {
        this.userDogs = [];
        this.cardLoiding = false;
      } else {
        this.userDogs = res.data;
        this.cardLoiding = false;
      }
    });
  }
  rejectMyDemand(demand) {
    this.userService.rejectMyAdoptDemand(demand.id_demand).subscribe((res) => {
      if (res) {
        demand['status'] = 'user-cancel';
      }
    });
  }
  goDogDescriptioPage(demand) {
    const slug = demand.name.toLowerCase();
    this.router.navigate([`dogs/${slug}`]);
  }
  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
