import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { KeepDemand } from 'src/app/Models/keep-demand';
import { KeepDog } from 'src/app/Models/keepDog';
import { Rate } from 'src/app/Models/rate';
import { CurrentUserAnnonceService } from 'src/app/services/current-user-annonce.service';
import { RateService } from 'src/app/services/rate.service';
import { UserPassAnnoncesService } from 'src/app/services/user-pass-annonces.service';

interface usersRate {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-past-annonce',
  templateUrl: './past-annonce.component.html',
  styleUrls: ['./past-annonce.component.scss']
})
export class PastAnnonceComponent implements OnInit {
  @Input() currentUser;
  userId: string;
  constructor(private datePipe: DatePipe,
    private rateService: RateService,
    private userCurrentAnnonceService: CurrentUserAnnonceService,
    private userPassAnnoncesService: UserPassAnnoncesService) { }

  myPassAnnonces: KeepDog[];
  demandesByAnnonce: KeepDemand[];
  today = new Date();
  myDate: string;
  finAnnonceDate: string;
  cardLoiding: boolean;
  demandLoading: boolean;
  showDemandInfo: boolean;

  allUserRatings: Rate[] = [];
  values = [];

  rates: usersRate[] = [
    { value: '1', viewValue: 'Tres mouvais' },
    { value: '2', viewValue: 'Pas terribles' },
    { value: '3', viewValue: 'Pas mal' },
    { value: '4', viewValue: 'Tres bien' },
    { value: '5', viewValue: 'Excelent' },
  ];
  ngOnInit(): void {
    this.userId = this.currentUser.data.user.id;
    this.myDate = this.datePipe.transform(this.today, 'yyyy-MM-dd');
    this.getUserPassAnnonces(this.userId);
  }

  /**
 * 
 * On recupere tous annonces actuelles 
 * avec la date debut < date now
 */

  getUserPassAnnonces(owner_id) {
    this.cardLoiding = true;
    const body = {
      date: this.myDate
    }
    return this.userPassAnnoncesService.getUserPassAnnonces(owner_id, body).subscribe((res: any) => {
      if (res.data && res.data.length > 0) {
        this.myPassAnnonces = res.data;
      } else {
        this.myPassAnnonces = [];
      }

      this.cardLoiding = false;
    });
  }

  /**
 * 
 * On recupere all demandes pour chaque annonce
 */
  getDemandByAnnonce(annonce) {
    this.demandLoading = true;
    const dog_id = annonce.dog_id
    this.userCurrentAnnonceService.getDemandeByAnnonce(dog_id).subscribe((res: any) => {
      if (res && res.data !== "pas de demandes en cour") {
        this.demandesByAnnonce = res.data;
        this.demandesByAnnonce.forEach(demand => {
          if (demand.status === 'accept') {
            this.checkToRate(annonce, demand);
          }
        })
      } else {
        this.demandesByAnnonce = [];
      }
      this.demandLoading = false;
    })
  }

  openDemandByAnnoce(annonce) {
    this.getDemandByAnnonce(annonce);
    this.showDemandInfo = true;
    const el = document.getElementById('target');
    el.scrollIntoView({ behavior: 'smooth' });
  }

  checkToRate(annonce: KeepDog, demand: KeepDemand) {
    this.finAnnonceDate = this.datePipe.transform(annonce.date_fin, 'yyyy-MM-dd');
    if (this.finAnnonceDate < this.myDate) {
      const body = {
        id_demand: demand.id_demand,
        serve_user_id: demand.user_id,
        owner_user_id: this.currentUser.data.user.id
      }
      this.userPassAnnoncesService.findRateByUser(body).subscribe((res: any) => {
        if (res && res.data === "pas de rate by this user") {
          demand.toRate = true;
        } else {
          demand.toRate = false;
          demand.myRateToUser = res.data[0].value;
        }
      });
    }
  }
  /**
   *  Noter User
   */
  sendUserRate(event, demand: KeepDemand) {
    const body = {
      id_demand: demand.id_demand,
      serve_user_id: demand.user_id,
      owner_user_id: this.currentUser.data.user.id,
      value: event.value.value
    }

    this.rateService.sendUserRate(body).subscribe((res: any) => {
      if (res) {
        demand.toRate = false;
        demand.myRateToUser = res.message.value;
        this.updateGlobalUserRate(demand)
      }
    });
  }

  /**
    *  Selon notre note on update rating global pour  user noté
    */
  updateGlobalUserRate(demand: KeepDemand) {
    this.values = [];
    this.rateService.getAllUserRating(demand.user_id).subscribe((res: any) => {
      this.allUserRatings = res.message;
      this.allUserRatings.forEach(rate => {
        this.values.push(rate.value);
      });
      const newRate = (this.values.reduce(function (sum, current) {
        return sum + current;
      }, 0)) / this.values.length;
      const body = {
        rating: newRate.toFixed(1)
      }
      this.rateService.updateGlobalUserRate(demand.user_id, body).subscribe(res => {
        demand.rating = newRate.toFixed(1);
      })
    })
  }

}
