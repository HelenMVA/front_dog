import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { KeepDemand } from 'src/app/Models/keep-demand';
import { KeepDog } from 'src/app/Models/keepDog';
import { CurrentUserAnnonceService } from 'src/app/services/current-user-annonce.service';
import { ModalSendMessageComponent } from '../../modal-send-message/modal-send-message.component';
import { ConfirmModalComponent } from "../../modals/confirm-modal/confirm-modal.component";
import { KeepDogService } from "../../../services/keep-dog.service";
import { ModalUserEditAnnonceComponent } from "../../modal-user-edit-annonce/modal-user-edit-annonce.component";

interface statusAnn {
    value: string;
    viewValue: string;
}


@Component({
    selector: 'app-current-annonce',
    templateUrl: './current-annonce.component.html',
    styleUrls: ['./current-annonce.component.scss']
})
export class CurrentAnnonceComponent implements OnInit {
    @Input() currentUser;
    userId: string;
    dogName: string
    cardLoiding: boolean;
    demandLoading: boolean;
    showDemandInfo: boolean;

    myActiveAnnonces: KeepDog[];
    filteredAnnonce: KeepDog[] = [];
    demandesByAnnonce: KeepDemand[];


    today = new Date();
    myDate: string
    statusAnnonce: statusAnn[] = [
        { value: 'all', viewValue: 'Tous annonces' },
        { value: '1', viewValue: 'Avalaible' },
        { value: '0', viewValue: 'Unavailable ' },
    ];

    constructor(private userCurrentAnnonceService: CurrentUserAnnonceService,
        private modalService: NgbModal,
        private keepService: KeepDogService,
        private toastService: ToastrService,
        private datePipe: DatePipe) {
    }

    ngOnInit(): void {
        this.userId = this.currentUser.data.user.id;
        this.myDate = this.datePipe.transform(this.today, 'yyyy-MM-dd');
        this.getUserAnnonces(this.userId);
    }

    /**
     *
     * On recupere tous annonces actuelles
     * avec la date debut < date now
     */

    getUserAnnonces(owner_id) {
        this.cardLoiding = true;
        const body = {
            date: this.myDate
        }
        return this.userCurrentAnnonceService.getUserCurrentAnnonces(owner_id, body).subscribe((res: any) => {
            if (res.data && res.data.length > 0) {
                this.myActiveAnnonces = res.data;
                this.filteredAnnonce = res.data;
            } else {
                this.myActiveAnnonces = [];
                this.filteredAnnonce = [];
            }
            this.cardLoiding = false;
        });
    }

    /**
     *
     * On recupere all demandes pour chaque annonce
     */
    getDemandByAnnonce(dog_id) {
        this.demandLoading = true;
        this.userCurrentAnnonceService.getDemandeByAnnonce(dog_id).subscribe((res: any) => {
            if (res && res.data !== "pas de demandes en cour") {
                // this.myActiveAnnonces
                this.demandesByAnnonce = res.data;
            } else {
                this.demandesByAnnonce = [];
            }
            this.demandLoading = false;
        });
    }

    openDemandByAnnoce(annonce) {
        this.getDemandByAnnonce(annonce.dog_id);
        this.dogName = annonce.name;
        this.showDemandInfo = true;
        const el = document.getElementById('target');
        el.scrollIntoView({ behavior: 'smooth' });
    }

    sendMessage(annonce: KeepDemand) {
        const modalRef = this.modalService.open(ModalSendMessageComponent);
        modalRef.componentInstance.userReceiverId = annonce.user_id;
        modalRef.componentInstance.firstname = annonce.firstname;
        modalRef.result.then((result) => {
            if (result && result != null) {
                this.toastService.success("votre message etait envoyé");
            }
        });
    }

    /**
     *
     * On valide User a garder le chien
     * O change status demande pour ne plu afucher les button reject/accept
     * SI STATUS ACCEPT =>
     * suit on change status de chien non avalaible
     * suis on refus les autre demandes a garder ce chien
     */
    editStatusDemand(demand: KeepDemand, status: string) {
        const body = {
            demandId: demand.id_demand,
            st: status
        }
        this.userCurrentAnnonceService.editStatusDemand(body).subscribe((res: any) => {
            demand.status = status;
            if (status && status === "accept") {
                this.editDogAvailability(demand.dog_id);
                this.demandesByAnnonce.forEach(dmd => {
                    if (dmd.id_demand !== demand.id_demand) {
                        this.editStatusDemand(dmd, 'reject');
                    }
                });
            }
        });
    }

    editDogAvailability(dog_id) {
        const body = {
            dog_id: dog_id
        }
        this.userCurrentAnnonceService.editDogAvailability(body).subscribe(res => {
            this.myActiveAnnonces.forEach((annonce: KeepDog) => {
                if (annonce.dog_id === dog_id) {
                    annonce.avalaible = false;
                }
            });
        });
    }

    /**
     * Filter
     */
    filterStatus(ev) {
        this.showDemandInfo = false;
        this.dogName = null;
        if (ev.value.value === '0' || ev.value.value === '1') {
            this.filteredAnnonce = this.myActiveAnnonces.filter(annonce => annonce.avalaible == ev.value.value);
        } else {
            this.filteredAnnonce = this.myActiveAnnonces;
        }
    }

    /**
     *
     * Delete User Annonce
     */
    deleteMyAnnonce(annonce) {
        const modalRef = this.modalService.open(ConfirmModalComponent);
        modalRef.componentInstance.text = "Voulez vous supprimer votre annonce? Il sera supprimé pour tous les utilisateurs ayant répondu ";
        modalRef.result.then((result) => {
            if (result && result === 'yes') {
                this.keepService.deleteMyAnnonce(annonce).subscribe((res: any) => {
                    //     /**
                    //      * DELETE de partie front
                    //      * apres avoir obtenu la resp
                    //      * pour ne pas reload page
                    //      */
                    const deletedAnnonce = this.myActiveAnnonces.findIndex(an => an.dog_id === annonce.dog_id);
                    this.myActiveAnnonces.splice(deletedAnnonce, 1);
                });
            }
        });
    }

    /**
     *
     *Edit User Annonce
     */
    editMyAnnonce(annonce: KeepDemand) {
        const modalRef = this.modalService.open(ModalUserEditAnnonceComponent);
        modalRef.componentInstance.annonce = annonce;
        modalRef.componentInstance.currentUser = this.currentUser.data.user;
        modalRef.result.then((result) => {
            console.dir(result);
        });
    }

}
