import { NgModule } from '@angular/core';
import { PipeModule } from 'src/app/pipes/pipe.module';


@NgModule({
    imports: [
        PipeModule
    ],
    declarations: [

    ],
    providers: [

    ]
})
export class CurrentAnnonceModule {
}