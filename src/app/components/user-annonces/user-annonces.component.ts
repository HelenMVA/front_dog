import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {KeepDog} from 'src/app/Models/keepDog';
import {User} from 'src/app/Models/user';
import {CurrentUserService} from 'src/app/services/current-user.service';
import {UserService} from 'src/app/services/user.service';

@Component({
    selector: 'app-user-annonces',
    templateUrl: './user-annonces.component.html',
    styleUrls: ['./user-annonces.component.scss']
})
export class UserAnnoncesComponent implements OnInit, OnDestroy {
    currentUserSubscription: Subscription;
    currentUser: User;
    userId: string;
    cardLoading: boolean;
    myAnnonces: KeepDog[] = [];

    constructor(private userService: UserService,
                private currentUserService: CurrentUserService,
                private router: Router) {
        this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
            (user: any) => {
                if (user != null) {
                    this.currentUser = user;
                    this.userId = user.data.user.id;
                }
            }
        );
    }

    ngOnInit(): void {
        window.scroll(0, 0);
    }

    getUsernnonces(ownerId) {
        this.cardLoading = true;
        return this.userService.getUsernnonces(ownerId).subscribe((res: any) => {
            this.myAnnonces = res.data;
            this.cardLoading = false;
        });
    }

    ngOnDestroy(): void {
        this.currentUserSubscription.unsubscribe();
    }
}
