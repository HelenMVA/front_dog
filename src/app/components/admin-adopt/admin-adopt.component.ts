import { Component, OnInit, Sanitizer } from '@angular/core'
import { DomSanitizer } from "@angular/platform-browser";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from 'src/app/services/admin.service';
import { ModalAddAdoptAnnounceAdminComponent } from '../modal-add-adopt-announce-admin/modal-add-adopt-announce-admin.component';
import { ModalInfoComponent } from '../modal-info/modal-info.component';
import { ModalSendMessageComponent } from '../modal-send-message/modal-send-message.component';
import { AdoptDemand } from '../../Models/adopt-demand';
import { ToastrService } from "ngx-toastr";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DropDown } from 'src/app/Models/drop-down';
import { UploadImgService } from 'src/app/services/upload-img.service';

interface Status {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-admin-adopt',
  templateUrl: './admin-adopt.component.html',
  styleUrls: ['./admin-adopt.component.scss'],
})
export class AdminAdoptComponent implements OnInit {
  allDemandAdoption: any[] = [];
  allFiltredDemandAdoption: any[] = [];
  showNewAnnonce: boolean = false;

  /**
   * IMG
   */
  url: any;
  FOLDER = 'furry_friends/';
  filekey: string;
  fileUrl = '  https://alyona.s3.eu-west-3.amazonaws.com/';
  selectedFiles: FileList;
  file;
  filekey1: string;
  selectedFiles1: FileList;
  file1;
  filekey2: string;
  selectedFiles2: FileList;
  file2;
  filekey3: string;
  selectedFiles3: FileList;
  file3;
  filekey4: string;
  selectedFiles4: FileList;
  file4;

  status: Status[] = [
    { value: 'tous', viewValue: 'All' },
    { value: 'accept', viewValue: 'Accepté' },
    { value: 'reject', viewValue: 'Refusé' },
    { value: 'pending', viewValue: 'pending' },
    { value: 'user-cancel', viewValue: 'user-cancel' }
  ];
  newAnnonce: FormGroup = this.fb.group({
    dogName: ['', Validators.compose([Validators.required])],
    age: ['', Validators.compose([Validators.required])],
    description: ['', Validators.compose([Validators.required])],
    race: ['', Validators.compose([Validators.required])],
    url: ['', Validators.compose([Validators.required])],
    url1: ['', Validators.compose([Validators.required])],
    url2: ['', Validators.compose([Validators.required])],
    url3: ['', Validators.compose([Validators.required])],
    url4: ['', Validators.compose([Validators.required])],
    class: ['', Validators.compose([Validators.required])],
    catLove: ['', Validators.compose([Validators.required])],
    dogLove: ['', Validators.compose([Validators.required])],
    childLove: ['', Validators.compose([Validators.required])],
    sexe: ['', Validators.compose([Validators.required])],
    characteristic: ['', Validators.compose([Validators.required])],
  });
  // DROP DOWN select

  classification: DropDown[];
  dogSexe: DropDown[];
  atherDog: DropDown[];
  cats: DropDown[];
  childs: DropDown[];
  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private imageUploadService: UploadImgService,
    private adminService: AdminService,
    private toastService: ToastrService
  ) {

    this.classification = [
      { value: '1', viewValue: 'Chiot' },
      { value: '2', viewValue: 'Junior' },
      { value: '3', viewValue: 'Adulte' },
      { value: '4', viewValue: 'Senior' },
    ];
    this.dogSexe = [
      { value: '1', viewValue: 'Male' },
      { value: '2', viewValue: 'Femelle' },
    ];
    this.atherDog = [
      { value: '0', viewValue: "Incompatible avec d' autres chiens " },
      { value: '1', viewValue: 'Compatible' },
    ];
    this.cats = [
      { value: '0', viewValue: 'Incompatible' },
      { value: '1', viewValue: 'Compatible' },
    ];
    this.childs = [
      { value: '0', viewValue: 'Incompatible' },
      { value: '1', viewValue: 'Compatible' },
    ];
  }

  ngOnInit(): void {
    this.getAllDemandes();
  }
  getAllDemandes() {
    this.adminService.getAllDemandes().subscribe((res: any) => {
      this.allDemandAdoption = res.message;
      this.allFiltredDemandAdoption = res.message;
    });
  }
  /**
   * 
   * On accepte le demande user pour adopter un chien
   * ensuite on appele function pour mette non valaible de cette chien dans la table adopt chien
   * ensuite on refuse les autre demandes pour cette chiens
   */
  acceptDemand(demand: AdoptDemand) {
    this.adminService.accept(demand.id_demand).subscribe(() => {
      demand.status = 'accept';
      this.changeAvailableDog(demand.id_dog);
      this.allDemandAdoption.forEach(el => {
        if (el.id_demand !== demand.id_demand && el.id_dog === demand.id_dog) {
          this.rejectDemand(el);
          // el.status = 'reject'
        }
      });
    });
    // this.getAllDemandes();
  }
  changeAvailableDog(dogId) {
    const dogAvalaible = {
      avalaible: 0
    }
    this.adminService.changeAvailableDog(dogId, dogAvalaible).subscribe(res => console.dir(res));
  }


  rejectDemand(demand: AdoptDemand) {
    this.adminService.reject(demand.id_demand).subscribe(() => {
      // this.getAllDemandes();
      demand.status = 'reject';
    });
  }

  sendMessage(demand) {
    const modalRef = this.modalService.open(ModalSendMessageComponent);
    modalRef.componentInstance.userReceiverId = demand.id;
    modalRef.componentInstance.firstname = demand.firstname;
    modalRef.result.then((result) => {
      if (result && result != null) {
        this.toastService.success("votre message etai envoyé");
      }
    });
  }
  openModalInfo(demand, parametr) {
    const modalRef = this.modalService.open(ModalInfoComponent, { size: 'sm' });
    modalRef.componentInstance.info = demand;
    modalRef.componentInstance.param = parametr;
  }

  openModalAddAnnounce() {
    // this.modalService.open(ModalAddAdoptAnnounceAdminComponent);
    this.showNewAnnonce = !this.showNewAnnonce;
  }

  /**
   * Filter
   */
  filterStatus(ev) {
    if (ev.value.value !== 'tous') {
      this.allFiltredDemandAdoption = this.allDemandAdoption.filter(demand => demand.status === ev.value.value);
    } else {
      this.allFiltredDemandAdoption = this.allDemandAdoption;
    }
  }
  /**
   * NEW ANNONCE
   */

  choseClassification(e) {
    // this.selectedClass = e.value.value,
    this.newAnnonce.controls.class.setValue(e.value.value);
  }

  choseDogSexe(e) {
    // this.selectedSexe = e.value.value,
    this.newAnnonce.controls.sexe.setValue(e.value.value);
  }

  choseLoveAutherDogs(e) {
    // this.dogLoveSelectes = e.value.value;
    this.newAnnonce.controls.dogLove.setValue(e.value.value);
  }

  choseLovaCat(e) {
    // this.catLoveSelected =e.value.value,
    this.newAnnonce.controls.catLove.setValue(e.value.value);
  }

  choseLoveChild(e) {
    // this.childLoveSelected = e.value.value;
    this.newAnnonce.controls.childLove.setValue(e.value.value);
  }

  selectFile(event) {
    console.dir(event);
    this.selectedFiles = event.target.files;
    console.dir(this.selectedFiles);
    this.file = this.selectedFiles.item(0);
    console.dir(this.file);
    this.filekey = this.FOLDER + (Date.now() + this.selectedFiles.item(0).name);
    console.dir(this.filekey)
    this.newAnnonce.controls.url.setValue(this.fileUrl + this.filekey);
    console.dir(this.newAnnonce);
  }
  selectFile1(event) {
    this.selectedFiles1 = event.target.files;
    this.file1 = this.selectedFiles1.item(0);
    this.filekey1 = this.FOLDER + (Date.now() + this.selectedFiles1.item(0).name);
    this.newAnnonce.controls.url1.setValue(this.fileUrl + this.filekey);
  }
  selectFile2(event) {
    this.selectedFiles2 = event.target.files;
    this.file2 = this.selectedFiles2.item(0);
    this.filekey2 = this.FOLDER + (Date.now() + this.selectedFiles2.item(0).name);
    this.newAnnonce.controls.url2.setValue(this.fileUrl + this.filekey);
  }
  selectFile3(event) {
    this.selectedFiles3 = event.target.files;
    this.file3 = this.selectedFiles3.item(0);
    this.filekey3 = this.FOLDER + (Date.now() + this.selectedFiles3.item(0).name);
    this.newAnnonce.controls.url3.setValue(this.fileUrl + this.filekey);
  }
  selectFile4(event) {
    this.selectedFiles4 = event.target.files;
    this.file4 = this.selectedFiles4.item(0);
    this.filekey4 = this.FOLDER + (Date.now() + this.selectedFiles4.item(0).name);
    this.newAnnonce.controls.url4.setValue(this.fileUrl + this.filekey);
  }

  sendNewAnnonce() {
    this.imageUploadService.uploadFile(this.file, this.filekey);
    this.imageUploadService.uploadFile(this.file1, this.filekey1);
    this.imageUploadService.uploadFile(this.file2, this.filekey2);
    this.imageUploadService.uploadFile(this.file3, this.filekey3);
    this.imageUploadService.uploadFile(this.file4, this.filekey4);
    const annonce = {
      name: this.newAnnonce.value.dogName,
      characteristic: this.newAnnonce.value.description,
      classification: this.newAnnonce.value.class,
      photo: this.newAnnonce.value.url,
      age: this.newAnnonce.value.age,
      sexe: this.newAnnonce.value.sexe,
      race: this.newAnnonce.value.race,
      slug: this.newAnnonce.value.dogName + Date.now(),
      love_dog: this.newAnnonce.value.dogLove,
      love_cat: this.newAnnonce.value.catLove,
      love_child: this.newAnnonce.value.childLove,
      img1: this.newAnnonce.value.url1,
      img2: this.newAnnonce.value.url2,
      img3: this.newAnnonce.value.url3,
      img4: this.newAnnonce.value.url4,
    }
    this.adminService.addAnnonce(annonce).subscribe(res => {
      console.dir('ok');
    })
  }
}
