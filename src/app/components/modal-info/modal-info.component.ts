import { Component, Input, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.component.html',
  styleUrls: ['./modal-info.component.scss'],
})
export class ModalInfoComponent implements OnInit {
  @Input() public param;
  @Input() public info;

  constructor(private router: Router,
              private activeModal: NgbActiveModal) { }

  ngOnInit(): void {

  }

  goDogDescriptionPage(info) {
    const slug = info.name.toLowerCase();
    this.router.navigate([`dogs/${slug}`]);
    this.activeModal.close();
  }
}
