import { Component, OnInit } from '@angular/core';
import { RegisterModalComponent } from '../register-modal/register-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConnectModalComponent } from '../connect-modal/connect-modal.component';
import { UserService } from 'src/app/services/user.service';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { User } from 'src/app/Models/user';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  /**
    * Subscriptions
    */
  currentUserSubscription: Subscription;
  currentUser;
  constructor(private modalService: NgbModal,
    private curretUserService: CurrentUserService,
    private toastService: ToastrService,
    private router: Router,
    private userService: UserService) {

    /**
  * on récupere User dans local storage pour verifier l'acces
  */
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.curretUserService.setCurrenUser(this.currentUser);

    this.currentUserSubscription = this.curretUserService.currentUser
      .subscribe(usr => {
        this.currentUser = usr;
      });

  }

  ngOnInit(): void {
  }

  showInscriptionModal() {
    this.modalService.open(RegisterModalComponent);
  }

  showConnectionModal() {
    const modalRef = this.modalService.open(ConnectModalComponent);
    modalRef.result.then((result) => {
      if (result && result !== null) {
        this.currentUser = result;
        this.toastService.success('Hello', this.currentUser.data.user.firstname);
        this.curretUserService.setCurrenUser(this.currentUser);
      }
    });
  }

  logOut() {
    this.curretUserService.setCurrenUser(null);
    this.userService.logOut();
    this.router.navigate(['/'])
  }


}
