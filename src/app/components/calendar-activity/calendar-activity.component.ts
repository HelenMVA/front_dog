import {Component, OnDestroy, OnInit} from '@angular/core';
import {CalendarOptions} from '@fullcalendar/angular';
import {Subscription} from 'rxjs';
import {CurrentUserService} from 'src/app/services/current-user.service';
import {UserService} from 'src/app/services/user.service';

@Component({
    selector: 'app-calendar-activity',
    templateUrl: './calendar-activity.component.html',
    styleUrls: ['./calendar-activity.component.scss']
})
export class CalendarActivityComponent implements OnInit , OnDestroy{
    currentUserSubscription: Subscription;
    currentUser;
    myKeepDogs = []
    event = [];
    calendarOptions: CalendarOptions = {
        initialView: 'dayGridMonth',
        themeSystem: 'Sketchy',
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            // right: 'dayGridMonth'
            right: ''
        },
        dayMaxEvents: false, // allow "more" link when too many events
        events: [
            // { title: 'event 1', start: '2020-08-12', end: '2020-08-20' },
            // { title: 'event 1', date: '2020-08-12' },
        ]
    };

    constructor(private currentUserService: CurrentUserService,
                private userService: UserService) {
        this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
            (user) => {
                this.currentUser = user;
            }
        );
    }

    ngOnInit(): void {
        this.getMyAcceptKeepDog();
    }

    getMyAcceptKeepDog() {
        this.userService.getMyAcceptKeepDog(this.currentUser.data.user.id).subscribe((res: any) => {
            if (res.data !== 'No accept keep dog by this user'){
                this.myKeepDogs = res.data;
                this.myKeepDogs.forEach(dog => {
                    const ev = {
                        title: dog.name,
                        start: dog.date_start,
                        end: dog.date_fin
                    };
                    this.event.push(ev);
                });
                this.calendarOptions.events = this.event;
            } else {
                this.calendarOptions.events = [];
            }

        });
    }

    ngOnDestroy(): void {
        this.currentUserSubscription.unsubscribe();
    }

}
