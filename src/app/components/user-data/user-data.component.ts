import { NullTemplateVisitor } from '@angular/compiler';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { rejects } from 'assert';
import { resolve } from 'dns';
import { ToastrService } from 'ngx-toastr';
import { promise } from 'protractor';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UploadImgService } from 'src/app/services/upload-img.service';
import { UserService } from 'src/app/services/user.service';
import { ConfirmModalComponent } from '../modals/confirm-modal/confirm-modal.component';

@Component({
    selector: 'app-user-data',
    templateUrl: './user-data.component.html',
    styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent implements OnInit, OnDestroy {
    selectedFiles: FileList;
    file;
    FOLDER = 'furry_friends/';
    filekey: string;
    fileUrl = 'https://alyona.s3.eu-west-3.amazonaws.com/';
    url: string;

    currentUserSubscription: Subscription;
    currentUser;
    userId: string;
    visibility: number;
    /**
     * Email validator
     */
    emailValidator: RegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    constructor(private userService: UserService,
        private toastService: ToastrService,
        private modalService: NgbModal,
        private currentUserService: CurrentUserService,
        private imageUploadService: UploadImgService,
        private router: Router) {
        this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
            (user: any) => {
                if (user != null) {
                    this.currentUser = user;
                    this.userId = user.data.user.id;
                }
            }
        );
    }

    ngOnInit(): void {
        window.scroll(0, 0);
    }

    onSubmit(user) {
        console.dir(user.form.value);
    }

    editAvatar() {
        console.dir('delete');
    }

    deleteAvatar() {
        this.userService.deleteAvatar(this.currentUser.data.user.id, null).subscribe((resp) => {
            this.currentUser.data.user.avatar_url = null;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
        });
    }

    selectFile(event) {
        this.selectedFiles = event.target.files;
        this.file = this.selectedFiles.item(0);
        this.filekey = this.FOLDER + (Date.now() + this.selectedFiles.item(0).name);

        this.url = this.fileUrl + this.filekey;
        const avatarUrl = (this.fileUrl + this.filekey);
        const url = {
            avatarUrl
        };

        this.imageUploadService.uploadFile(this.file, this.filekey).then(() => {
            this.userService.updateAvatar(this.currentUser.data.user.id, url).subscribe((res: any) => {
                this.currentUser.data.user.avatar_url = res.message;
                this.currentUserService.setCurrenUser(this.currentUser);
                localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
            });
        }).catch((err) => console.log(err));
    }

    /**
     * UPDATE user firstname /lastname
     */

    sendUserData(data: any) {
        const url = {
            firstname: data.firstname,
            lastname: data.lastname,
        };
        this.userService.editUserData(data, this.currentUser.data.user.id).subscribe((res: any) => {
            if (res && res.success) {
                this.currentUser.data.user.firstname = data.firstname;
                this.currentUser.data.user.lastname = data.lastname;
                this.currentUserService.setCurrenUser(this.currentUser);
                localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                this.toastService.success('vos donnes etait modifiés ');
            }
        });
    }

    sendUserEmail(data: any) {
        const email = {
            email: data.email,
        };
        const emailStorage = JSON.parse(localStorage.getItem('currentUser')).data.user.email
        if (data.email !== emailStorage) {
            this.userService.editUserEmail(email, this.currentUser.data.user.id).subscribe(res => {
                if (res) {
                    this.currentUser.data.user.email = data.email;
                    this.currentUserService.setCurrenUser(this.currentUser);
                    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                    this.toastService.success('email est modifié');
                }
            },
                error => this.toastService.error('Cet email ne peut pas être utilisé'));
        } else {
            this.toastService.error('Vous avez pas modifié votre email');
        }

    }
    /**
     * UPDATE user visibility
     */
    changeVisibility(e) {
        const modalRef = this.modalService.open(ConfirmModalComponent);
        modalRef.componentInstance.text = "Voulez vous chager la visibilite votre profil?";
        modalRef.result.then((result) => {
            if (result && result === 'yes') {
                if (e.checked === true) {
                    this.visibility = 1;
                } else if (e.checked === false) {
                    this.visibility = 0;
                }
                const body = {
                    visibility: this.visibility,
                }
                this.userService.changeVisibility(body, this.currentUser.data.user.id).subscribe((res: any) => {
                    if (res && res.success) {
                        this.currentUser.data.user.visibility = e.checked;
                        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                        this.toastService.success('visibilite etait modifie')
                    }
                });
            }
        });
    }


    ngOnDestroy(): void {
        this.currentUserSubscription.unsubscribe();
    }
}
