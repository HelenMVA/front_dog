export interface User {
    data: any;
    id: number;
    firstname: string;

    lastname: string;
    user_role: string;

    email: string;

    password: string;

    avatar_url: string;
    visibility: number;
    blocked: boolean;
    created_date: Date;
    rating: number;
    token?: string;


}
