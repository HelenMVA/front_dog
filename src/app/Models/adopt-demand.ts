export interface AdoptDemand {
    id_demand: string;
    id_dog: string;
    id_user: string;
    phone: string;
    reason: string;
    status: string
}
