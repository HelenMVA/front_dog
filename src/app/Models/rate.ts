export interface Rate {
    owner_user_id: number,
    serve_user_id: number,
    id_deman: number,
    value: number,
    date: Date
}
