export interface UserMessages {
    avatar_url: string;
    from_user_id: string;
    id_message: string;
    message: string;
    timestamp: Date;
    to_user_id: string;
}
