export interface MyUsers {
    id: string;
    firstname: string;
    lastname: string;
    avatar_url: string;
    active: boolean;
}
