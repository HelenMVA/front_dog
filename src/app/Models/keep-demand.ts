export interface KeepDemand {
    id_demand: string;
    dog_id: string;

    user_id: string;
    firstname: string;
    lastname: string;
    avatar_url: string;
    rating: string;
    created_date: Date;
    nbKeepDog: number;

    toRate: boolean;
    myRateToUser: number;
    status: string;
}
