import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { AdoptService } from 'src/app/services/adopt.service';
import { DogsService } from 'src/app/services/dogs.service';
import { UserService } from 'src/app/services/user.service';
import { trigger, transition, style, animate } from '@angular/animations';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-adopt-dog-description',
  templateUrl: './adopt-dog-description.component.html',
  styleUrls: ['./adopt-dog-description.component.scss'],
  animations: [
    trigger('inGoAnimation', [
      transition(':enter', [
        style({ transform: 'translateY(0%)', opacity: 0 }),
        animate('400ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
    ]),
  ],
})
export class AdoptDogDescriptionComponent implements OnInit, OnDestroy {
  currentUserSubscription: Subscription;

  slug: string;
  dog: any;
  currentUser;
  myDemands: [];

  isShow1: boolean = true;
  isShow2: boolean = false;
  isShow3: boolean = false;
  isShow4: boolean = false;
  adoptForm: boolean = false;
  userHasDemand: boolean = true;

  cardLoiding: boolean;

  FormAdoption: FormGroup = this.fb.group({
    phone: ['', Validators.required],
    reason: ['', Validators.required],
  });

  constructor(
    private route: ActivatedRoute,
    private toastService: ToastrService,
    private dogService: DogsService,
    private fb: FormBuilder,
    private adoptService: AdoptService,
    private currentUserService: CurrentUserService,
    private userService: UserService
  ) {
    this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
      (user) => {
        this.currentUser = user;
        if (this.currentUser) {
          this.getMyDemandAdopt()
        }
      }
    );
  }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.slug = this.route.snapshot.paramMap.get('slug');
    this.getDog(this.slug);
  }

  getDog(slug) {
    this.cardLoiding = true;
    return this.dogService.getDog(slug).subscribe((res: any) => {
      this.dog = res.data[0];
      this.cardLoiding = false
      if (this.currentUser) this.getMyDemandAdopt()
    });
  }

  /**********
   * ***** SLIDER
   * ********/
  goPhoto2() {
    this.isShow1 = false;
    this.isShow2 = true;
    this.isShow3 = false;
    this.isShow4 = false;
  }
  goPhoto3() {
    this.isShow1 = false;
    this.isShow2 = false;
    this.isShow3 = true;
    this.isShow4 = false;
  }
  goPhoto4() {
    this.isShow1 = false;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = true;
  }
  goPhoto1() {
    this.isShow1 = true;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = false;
  }

  /**********
   * ***** show form Demande l'adoptation
   * ********/
  showForm() {
    this.adoptForm = true;
  }
  closeForm() {
    this.adoptForm = false;
  }
  /**
   * Send demande
   */
  sendDemande() {
    const id_user = this.currentUser.data.user.id;
    const id_dog = this.dog.dog_id;
    const phone = this.FormAdoption.controls.phone.value;
    const reason = this.FormAdoption.controls.reason.value;
    const dog = {
      id_dog,
      id_user,
      phone,
      reason,
    };
    this.adoptService.adopter(dog).subscribe(
      (data) => {
        this.getMyDemandAdopt();
        this.adoptForm = false;
        this.toastService.success("Votre demande d'adoption est pris en charge");
      },
      (err) => {
        // this.toastService.error(err);
        console.dir(err);
      }
    );
  }
  getMyDemandAdopt() {
    return this.userService.getUserDods(this.currentUser.data.user.id).subscribe((res: any) => {
      if (res && res.data !== 'No dog by this user') {
        this.myDemands = res.data;
        if (this.myDemands && this.myDemands.length >= 1) {
          this.myDemands.forEach((demand: any) => {
            if (demand && this.dog && demand.id_dog === this.dog.dog_id && demand.id_user === this.currentUser.data.user.id && demand.status === 'pending') {
              this.userHasDemand = false;
            }
          })
        }
      } else {
        this.myDemands = [];
        this.userHasDemand = true;
      }

    });
  }


  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
