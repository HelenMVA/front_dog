import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserService } from 'src/app/services/user.service';
import { AdminService } from '../../services/admin.service';
import { ModalSendMessageComponent } from "../../components/modal-send-message/modal-send-message.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";

@Component({
    selector: 'app-my-profil',
    templateUrl: './my-profil.component.html',
    styleUrls: ['./my-profil.component.scss'],
})
export class MyProfilComponent implements OnInit, OnDestroy {
    currentUserSubscription: Subscription;
    currentUser;
    userId: string;

    adminId: string;

    constructor(private userService: UserService,
        private router: Router,
        private modalService: NgbModal,
        private adminService: AdminService,
        private toastService: ToastrService,
        private currentUserService: CurrentUserService) {
        this.currentUserSubscription = this.currentUserService.currentUser.subscribe(
            (user: any) => {
                if (user != null) {
                    this.currentUser = user;
                    this.userId = user.data.user.id;
                }
            }
        );
    }

    ngOnInit(): void {
        this.getAdmin();
    }

    getAdmin() {
        this.adminService.getAdmin().subscribe((res: any) => {
            this.adminId = res.message[0].id;
        });
    }

    showModalSendMessage() {
        const modalRef = this.modalService.open(ModalSendMessageComponent);
        modalRef.componentInstance.userReceiverId = this.adminId;
        modalRef.componentInstance.admin = 'ADMIN';

        modalRef.result.then((result) => {
            if (result && result != null) {
                this.toastService.success('votre message etait envoyé');
            }
        });
    }

    ngOnDestroy(): void {
        this.currentUserSubscription.unsubscribe();
    }
}
