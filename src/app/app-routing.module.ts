import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminAdoptComponent } from './components/admin-adopt/admin-adopt.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { UserAdoptRequestComponent } from './components/user-adopt-request/user-adopt-request.component';
import { UserAnnoncesComponent } from './components/user-annonces/user-annonces.component';
import { UserDataComponent } from './components/user-data/user-data.component';
import { UserKeepRequestComponent } from './components/user-keep-request/user-keep-request.component';
import { UserMessagesComponent } from './components/user-messages/user-messages.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AdoptDogDescriptionComponent } from './pages/adopt-dog-description/adopt-dog-description.component';
import { AdoptPageComponent } from './pages/adopt-page/adopt-page.component';
import { GardePageComponent } from './pages/garde-page/garde-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MyProfilComponent } from './pages/my-profil/my-profil.component';
import { CurrentUserGuard } from './services/guard/current-user.guard';
import {AdminGuard} from "./services/guard/admin.guard";

const routes: Routes = [
  { path: '', component: HomePageComponent },
  {
    path: 'dogs',
    component: AdoptPageComponent,

    children: [
      {
        path: ':slug',
        component: AdoptDogDescriptionComponent,
      },
    ],
  },
  { path: 'garde', component: GardePageComponent },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminGuard],
    children: [
      {
        path: 'adopt',
        component: AdminAdoptComponent,
      },
      {
        path: 'users',
        component: AdminUsersComponent,
      },
      {
        path: 'message',
        component: UserMessagesComponent,
      },
    ],
  },
  {
    path: 'profil',
    component: MyProfilComponent,
    canActivate: [CurrentUserGuard],
    children: [
      {
        path: 'me',
        component: UserDataComponent,
      },
      {
        path: 'message',
        component: UserMessagesComponent,
      },
      {
        path: 'my-adopt',
        component: UserAdoptRequestComponent,
      },
      {
        path: 'my-keep',
        component: UserKeepRequestComponent,
      },
      {
        path: 'my-annonce',
        component: UserAnnoncesComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
