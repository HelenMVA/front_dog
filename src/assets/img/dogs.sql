-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 30 sep. 2020 à 22:06
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ohmydog`
--

-- --------------------------------------------------------

--
-- Structure de la table `adoption`
--

CREATE TABLE `adoption` (
  `dog_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `age` varchar(250) NOT NULL,
  `photo` varchar(250) NOT NULL,
  `description` varchar(2500) NOT NULL,
  `sex` varchar(25) NOT NULL,
  `race` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `classement` int(11) NOT NULL,
  `img1` varchar(2500) NOT NULL,
  `img2` varchar(2500) NOT NULL,
  `img3` varchar(2500) NOT NULL,
  `img4` varchar(2500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `adoption`
--

INSERT INTO `adoption` (`dog_id`, `name`, `age`, `photo`, `description`, `sex`, `race`, `slug`, `classement`, `img1`, `img2`, `img3`, `img4`) VALUES
(1, 'joujou', '2 ans', 'https://cdn.pixabay.com/photo/2017/10/02/21/56/dog-2810484_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'female', 'test', 'joujou', 6, '', '', '', ''),
(2, 'Bobby', '5 ans', 'https://cdn.pixabay.com/photo/2018/03/18/18/06/australian-shepherd-3237735_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'male', 'test', 'bobby', 6, '', '', '', ''),
(3, 'Djoh', '7 mois', 'https://labrador.ru/ipb/uploads/monthly_2018_08/GO7C9823-ev.jpg.8b8a288ebe4830bddbe7477b2d32bafb.jpg', 'Mon histoire n\'a pas bien commencée, J\'ai été trouvé abandonnée dans un sac plastique devant un conteneur à poubelle avec mes frères et sœurs, par une dame qui ne pouvait pas nous garder. Je suis très joueur, gentil, attachant, et très sociable,\r\nVenez me rencontrer vous allez craquer !!!', 'male', 'labrador', 'djoh', 6, 'https://labrador.ru/ipb/uploads/monthly_2018_08/GO7C9823-ev.jpg.8b8a288ebe4830bddbe7477b2d32bafb.jpg', 'https://labrador.ru/ipb/uploads/monthly_2020_02/0D0AAD22-942D-4814-B73B-6A32F3282488.jpeg.288244eeca607013bfbf5e6e85a58f4b.jpeg', 'https://petstime.ru/sites/default/files/inline/images/23432.jpg', 'https://avatars.mds.yandex.net/get-pdb/1927647/0b1e0521-7bf7-45a5-8735-0db2e02dcd10/s1200?webp=false'),
(4, 'Bob', '9 ans', 'https://cdn.pixabay.com/photo/2015/06/24/13/32/dog-820014_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'male', 'labrador', 'bob', 6, 'https://xn--80akenilasu.xn--p1ai/sysfiles/Afra_013.jpg', 'https://avatars.mds.yandex.net/get-pdb/2796595/2f5134a3-358f-4d08-82c1-cad1fd97d4ec/s1200?webp=false', 'https://yandex.ru/images/_crpd/4FhZy7232/01d910UB/p19bt5uugA2kvFmjMHA7GlcYe9GUFXi_26AVtImwKUv1Oduyv1biphcECBieZdFbsL5MkztUvv0u6dxFlm2hVXZeOtbqKhU5sb084iQk7qpC3a9UofA9Hy8Q1OAdIEnbgbsoAImxRCFxA_9-_NQkfgVuSDKOoNhr7-ybZVXp56CXLNjyLWgjTpe8GLESRclY9L2bYUQX2q98I4Sk7xALeuLoinG4wgVgAlJ_70y3Jr3nPMs2r0K6mqlMKxRUCjQS1r_a0v3pMx1WjQug0mZp6SVN2ICn1Ut8DDG2t04zeWhBO5jCuLEVsfNhfV6M1VbuR05rNI-g-crLz0nFBcg1c_cfCfIuCWNcpL364SEViQhGjqpTdgZo_y1SBRbeIgvp82gbw4nzNnABcx9PPkTlz8Vdi2Ve47g4KO-7V3R5BpB2_DrATThhjDbtm4FhBihbdEzb0zeFuvz_s1WF3jM5KFMLWUOrk3fCAsH9_M819qyVX5jn7tNbaPtsWmTU66eyhD3Zgqwb8wzWP3iAQWa7qtR-uTAGNwiOD7KFVS3ACpuR2vii-0OmcKBybEzcddYMNd17VJ2wCRu5zzu3FpjnQja8CnEtChAMZu2o8MG0W-gnr4qzpecbrz_AN7dMcEl6ELj7QMljlaCA4Dw-vWXmflU9GTVdwtiLm33bZcZ7pxD1Ptvij_lBHKR8KYIh1EqIRXzZA3W3KXwM4GbVDWF7yFL6CwOYsUSREPIPjT5kJ-9nXHul_iFLuNr9KFUW6gdgt2ybAX0r0t70f_ty4XYbaXZviWEHVKttb4IFFo-Ca_mBavlB6KHmk-BQHW99B3SuNk4JBN9guIqbfBvVJsh3AZTMihDtuTEc5yyJYTE0WJuXHqhz1eS4z14h9XV-IniLwHiKoIlRFnKAA12crmV1_Hf--UVvw1u6un4ZZWSZtuOlftsSjbgxjUZsmVMxZAlpFn2bQpaWaez9srSnn5MbI', 'https://cdn.pixabay.com/photo/2016/02/11/16/58/dog-1194079_1280.jpg'),
(5, 'Marley', '5 mois', 'https://cdn.pixabay.com/photo/2017/03/27/13/23/animal-2178696_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'male', 'staff terrier', 'marley', 6, '', '', '', ''),
(6, 'King', '10 mois', 'https://cdn.pixabay.com/photo/2016/07/07/15/35/puppy-1502565_960_720.jpg', 'Comme le dit mon nom, je suis un vrai King. Malgré ma taille et mon jeune âge, je n\'ai peur de rien. J\'adore jouer avec tout le monde et aussi avec mes congénères. Je suis très câlin et je suis sûre que vous aimeriez ma compagnie.', 'female', 'mops', 'puppy', 6, 'https://usatiki.ru/files/images/4279051374_afdee3a409_b.jpg', 'https://sun9-69.userapi.com/c850036/v850036683/c7358/Z6xscP8kGs4.jpg', 'https://grandgames.net/puzzle/source/grustnij_mops.jpg', 'https://world-dogs.ru/wp-content/uploads/2020/04/mops.jpg'),
(7, 'DouDou', '8 ans', 'https://cdn.pixabay.com/photo/2017/10/29/18/00/chihuahua-2900362_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'female', 'Spitz', 'doudou', 6, '', '', '', ''),
(8, 'Danny', '3 ans', 'https://cdn.pixabay.com/photo/2013/12/28/19/28/animal-portrait-234836_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'male', 'test', 'danny', 6, '', '', '', ''),
(9, 'Marley', '5 mois', 'https://cdn.pixabay.com/photo/2017/05/09/23/02/dog-2299482_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'male', 'test', 'marley', 6, '', '', '', ''),
(10, 'Lilou', '3 mois', 'https://cdn.pixabay.com/photo/2020/03/31/19/20/dog-4988985_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'female', 'Corgi', 'lilou', 6, 'https://vashkinolog.com/wp-content/uploads/2019/09/festival_sobak_velsh-korgi_v_loft_proekt_etazhi.jpg', 'https://fotovmire.ru/wp-content/uploads/2019/04/12771/sobaka-uvidela-chto-to-vdali.jpg', 'https://classpic.ru/wp-content/uploads/2019/09/36937/sobachka-smotrit-v-kameru.jpg', 'https://classpic.ru/wp-content/uploads/2019/09/36937/sobaka-korgi-vnimatelno-za-chem-to-nabljudaet.jpg'),
(11, 'Boss', '8 ans', 'https://cdn.pixabay.com/photo/2018/03/08/23/14/dalmatians-3210166_960_720.jpg', ' is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'male', 'Dalmatien', 'boss', 6, 'http://udivitelno.com/images/14/poroda-sobak-dalmatinets-opisanie-20-foto/13-%D0%94%D0%B0%D0%BB%D0%BC%D0%B0%D1%82%D0%B8%D0%BD%D1%86%D1%8B-%D0%BF%D1%80%D0%B5%D0%BA%D1%80%D0%B0%D1%81%D0%BD%D1%8B%D0%B5%20%D0%B0%D0%BA%D1%82%D0%B5%D1%80%D1%8B.jpg', 'https://i.pinimg.com/originals/dd/3a/5b/dd3a5bb081cfa048c35671a2496dc074.jpg', 'http://komotoz.ru/photo/porody_sobak/photos/dalmatin/dalmatin_03.jpg', 'https://avatars.mds.yandex.net/get-pdb/1899866/afb2c367-ba22-4227-95b6-b0c17b8f123d/s1200?webp=false');

-- --------------------------------------------------------

--
-- Structure de la table `classification`
--

CREATE TABLE `classification` (
  `id` int(11) NOT NULL,
  `name_class` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `classification`
--

INSERT INTO `classification` (`id`, `name_class`) VALUES
(8, 'adulte'),
(6, 'chiot'),
(7, 'junior'),
(9, 'senior');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `message_sender_id` int(11) NOT NULL,
  `message_receiver_id` int(11) NOT NULL,
  `message_body` varchar(2500) NOT NULL,
  `creation_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `user_role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `user_role`) VALUES
(23, 'Elena', 'Moiseeva', 'helen_moiseeva@yahoo.com', '$2b$10$tU4UORuKo6mbcnI1/LrlqexmFU9SAqA1HBarNn1dUlpzwdBhRCSw2', 'admin'),
(24, 'Alex', 'Luquet', 'aluq93@gmail.com', '$2b$10$67AOtiZ/dol/itwLxXGYHeUng8zkyumr7pu/CYy0LWrU6MTsxX1s2', 'user'),
(25, 'test', 'test', 'test@test.ru', 'test', 'user'),
(26, 'Anna', 'Golovanova', 'anna@daSobaka.ru', 'AZERTY', 'user');

-- --------------------------------------------------------

--
-- Structure de la table `user_has_adoption`
--

CREATE TABLE `user_has_adoption` (
  `id_demand` int(11) NOT NULL,
  `id_dog` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `phone` varchar(250) NOT NULL,
  `reason` varchar(250) NOT NULL,
  `status` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user_has_adoption`
--

INSERT INTO `user_has_adoption` (`id_demand`, `id_dog`, `id_user`, `phone`, `reason`, `status`) VALUES
(10, 3, 23, '1234567890', 'fghjklmlkjhgf', 'reject'),
(12, 6, 23, '098765432', 'allalalal', 'accept'),
(13, 11, 23, '234567867543RE', 'kabskjakjzdbzdfbgk', 'reject'),
(16, 6, 24, '12123456457687', 'bkababkakbab', 'reject'),
(17, 7, 23, '865434231422346', 'parce que', 'pending');

-- --------------------------------------------------------

--
-- Structure de la table `user_has_message`
--

CREATE TABLE `user_has_message` (
  `id_message` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `message` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user_has_message`
--

INSERT INTO `user_has_message` (`id_message`, `from_user_id`, `to_user_id`, `message`) VALUES
(1, 24, 23, 'Hi :)'),
(2, 24, 23, 'blablblabla'),
(3, 23, 24, 'Bonjour mon Gros'),
(4, 25, 23, 'приветулички как делишечки ?'),
(5, 23, 26, 'как дела?'),
(6, 25, 26, 'laalalalal');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adoption`
--
ALTER TABLE `adoption`
  ADD PRIMARY KEY (`dog_id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `classement` (`classement`);

--
-- Index pour la table `classification`
--
ALTER TABLE `classification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name_class` (`name_class`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `message_sender_id` (`message_sender_id`),
  ADD KEY `message_receiver_id` (`message_receiver_id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_role` (`user_role`) USING BTREE;

--
-- Index pour la table `user_has_adoption`
--
ALTER TABLE `user_has_adoption`
  ADD PRIMARY KEY (`id_demand`);

--
-- Index pour la table `user_has_message`
--
ALTER TABLE `user_has_message`
  ADD PRIMARY KEY (`id_message`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adoption`
--
ALTER TABLE `adoption`
  MODIFY `dog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `classification`
--
ALTER TABLE `classification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pour la table `user_has_adoption`
--
ALTER TABLE `user_has_adoption`
  MODIFY `id_demand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `user_has_message`
--
ALTER TABLE `user_has_message`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `adoption`
--
ALTER TABLE `adoption`
  ADD CONSTRAINT `adoption_ibfk_1` FOREIGN KEY (`classement`) REFERENCES `classification` (`id`);

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`message_sender_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`message_receiver_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `role` (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
