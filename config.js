const config = {
    APP_BASE_URL: process.env.API_URL || 'http://localhost:8000',
    ACCESS_KEY: process.env.ACCESS_KEY,
    SECRET_KEY: process.env.SECRET_KEY,
}

module.exports = config